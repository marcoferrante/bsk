package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.BowlingException;


public class FrameTest {

	@Test
	public void inputFirstThrow() throws Exception{
		int firstThrow = 1, secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertTrue(firstThrow == frame.getFirstThrow());
	}
	
	@Test(expected = BowlingException.class)
	public void frameShouldRaiseExceptionForMoreOf10Pins() throws Exception{
		Frame frame = new Frame(1,10);
	}
	
	@Test(expected = BowlingException.class)
	public void firstThrowShouldRaiseExceptionForLessOf0Pin() throws Exception{
		Frame frame = new Frame(-1,1);
	}
	
	@Test(expected = BowlingException.class)
	public void secondThrowShouldRaiseExceptionForLessOf0Pin() throws Exception{
		Frame frame = new Frame(1,-1);
	}
	
	@Test
	public void inputSecondThrow() throws Exception{
		int firstThrow = 1, secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertTrue(secondThrow == frame.getSecondThrow());
	}

	@Test
	public void scoreShouldBeSumOfThrows() throws Exception{
		int firstThrow = 1, secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertTrue(frame.getScore() == firstThrow + secondThrow);
	}

	@Test
	public void shouldBeSpare() throws Exception{
		Frame frame = new Frame(3, 7);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void shouldBeStrike() throws Exception{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike
				());
	}

	
	
	
}
