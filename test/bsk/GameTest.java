package bsk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void inputFrame() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(1, 2));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(3, 5));

		assertTrue(game.getFrameAt(3).equals(new Frame(4, 5)));
	}

	@Test
	public void calculateScore() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(1, 2));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(3, 5));

		assertTrue(game.calculateScore() == 68);
	}

	@Test
	public void inputBonus() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 7));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(1, 2));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(3, 5));

		assertTrue(game.getFrameAt(1).getBonus() == 7);

	}

	@Test
	public void calculateScoreWithBonusSpare() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 7)); // spare
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(1, 2));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(3, 5));

		assertTrue(game.calculateScore() == 76);
	}

	@Test
	public void calculateScoreWithBonusStrike() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(10, 0)); // strike
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(1, 2));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(3, 5));

		assertTrue(game.calculateScore() == 78);
	}

	@Test
	public void calculateScoreWithBonusStrikeFollowedFromSpare() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(10, 0)); // strike
		game.addFrame(new Frame(7, 3)); // spare
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(1, 2));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(3, 5));

		assertTrue(game.calculateScore() == 84);
	}

	@Test
	public void calculateScoreWithBonusMultipleStrike() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(10, 0)); // strike
		game.addFrame(new Frame(10, 0)); // strike
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));

		assertTrue(game.calculateScore() == 112);
	}

	@Test
	public void calculateScoreWithBonusMultipleSpare() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(8,2)); //spare
		game.addFrame(new Frame(5,5)); //spare
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));

		assertTrue(game.calculateScore() == 98);
	}
	
	@Test
	public void calculateScoreWithBonusSpareAtTheLast() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));	//spare
		game.setFirstBonusThrow(7);

		assertTrue(game.calculateScore() == 90);
	}
	
	@Test
	public void inputFirstThrowBonus() throws Exception{
		Game game = new Game();
		game.setFirstBonusThrow(7);

		assertTrue(game.getFirstBonusThrow() == 7);
	}
	
	@Test
	public void calculateScoreWithBonusStrikeAtTheLast() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(1,5)); 
		game.addFrame(new Frame(3,6)); 
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10,0)); //strike
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);

		assertTrue(game.calculateScore() == 92);
	}
	
	@Test
	public void inputSecondThrowBonus() throws Exception{
		Game game = new Game();
		game.setSecondBonusThrow(7);

		assertTrue(game.getSecondBonusThrow() == 7);
	}
	
	
	@Test
	public void calculateBestScore() throws Exception {

		Game game = new Game();
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		game.addFrame(new Frame(10,0)); //strike 
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);

		assertTrue(game.calculateScore() == 300);
	}
	
}
