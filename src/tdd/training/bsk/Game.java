package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	static int i = 0;
	private Frame[] frame = new Frame[10];
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		i = 0;
		for (int i = 0; i < 10; i++)
			try {
				frame[i] = new Frame(0, 0);
			} catch (BowlingException e) {
				e.printStackTrace();
			}
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frame[i] = frame;

		if (i > 0 && this.frame[i - 1].isSpare())
			this.frame[i - 1].setBonus(this.frame[i].getFirstThrow());
		if (i > 0 && this.frame[i - 1].isStrike())
			this.frame[i - 1].setBonus(this.frame[i].getFirstThrow() + this.frame[i].getSecondThrow());

		if (i > 1 && this.frame[i - 1].isStrike() && this.frame[i - 2].isStrike()) {
			this.frame[i - 2].setBonus(this.frame[i].getFirstThrow() + this.frame[i - 1].getFirstThrow());
			if (i == 9 && this.frame[i - 1].isStrike() && this.frame[i].isStrike())
				this.frame[i].setBonus(this.getFirstBonusThrow() + this.frame[i].getFirstThrow());

		}
		i++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return this.frame[index];
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int totalScore = 0;
		for (int i = 0; i < 10; i++) {
			totalScore += (this.frame[i].getScore());
		}
		return totalScore + this.getFirstBonusThrow() + this.getSecondBonusThrow();
	}

}
